Poderes de la Union 

Los poderes de la Uion estan conformados por el Poder Ejecutivo, Legistlativo y Judicial y estos son los encargados de proporcionar
equilibrio y contrapeso, para el buen funcionamiento del país 

Caracteristicas 

Poder Ejecutivo: 
1.- Recae en un solo individuo (Presidente)
2.- Dura en su cargo 6 años
3.- No reelección 
4.- Facultades 
4.1.- Promulgar 
4.2.- Nombrar 
4.3.- Disponer 

Poder Legislativo:
1.- Bi-camaral, Diputados y Senadores
2.- 300 diputados electos por votación mayoría relativa y 200 electos por representación proporcional mediante el sistema de listas
regionales
3.- Durarán en su cargo tres años y podran postularse asta por cuatro peridodos 
4.- Facultades para realizar una iniciativa de ley 

Poder Judicial:
1.- Aplicación adecuada de la norma jurídica 
2.- Se ecuentra integrada por 
2.1.- Suprema Corte de Juticia de la Nación 
2.2.- Tribunal Electoral 
2.3.- Tribunales Colegiados de Circuito
2.4.- Tribunales Unitarios de Circuito
2.5.- Juzgados de Distritos 

Organización 

Poder Ejecutivo
El estado se encuentra dividido, en virtud de que no puede estar en un solo miembro, ya que este tiende a corromperse por lo que 
el poder ejecutivo esta a cargo de una sola persona que depende de los otros dos poderes 

Poder Legislativo 
El estado crea la leyes para encausar la coducta del ser humano, por lo que a traves de una iniciativa de la ley que presenta
al Congreso de la Unión, el cual discute, aprueba y se le envía al Ejecutivo federal para que las sanciones; posteriormente la promulga
y ordena su publicación en el Diario Oficial de la Federación para su entrada en vigor

Poder Judicial 
La Suprema Corte de Justicia de la nación y los Tribunales Colegiados de Circuito son los unicos encargados de elaborar jurisprudencia,
apartir de cinco casos idénticos y una tesis en contra 

Funciones

Poder Ejecutivo

encargado de la administración de la federación así como también entablar relaciones internacionales.
El Poder Judicial es el conjunto de de tribunales federales estructurados jerárquicamente, estos son:
•	Suprema Corte de Justicia de la Nación 
•	Tribunal Electoral 
•	Congreso de la Judicatura Federal 

Poder Legislativo 
Es el encargado de crear normas y leyes; se le conoce como bi-camaral ya que se divide en una Cámara de Diputados y una Cámara de
Senadores.

Poder Judicial 
Cuya función primordial es la aplicación adecuada de la norma jurídica. 
La suprema Corte de Justicia de la Nación a los Tribunales Colegiados de Circuito son los únicos encargados de elaborar 
jurisprudencias, a partir de cinco casos idénticos y una tesis en contra.

Facultades 

Poder Ejecutivo 
Las faculades del presidente se pueden clacificar en: 
a) Facultad de promulgar: El Ejucutivo autentifica la existencia y regularidad de la ley, ordena su publicación y manda a usu
agentes que la hagan cumplir 
b) Facultad de ejecución: consiste en la realización de actos necesarios,para hacer efectiva, en casos concretos, la ley que emana
del Congreso de la Unión por lo que el desarrollo de estos actos inicia después de la
promulgación.
c) Facultad de nombramiento: Es aquella que consiste en designar al personal que colaborará en el ejercicio de su cargo.

Poder Legislativo
Cámara de Diputados: en el art. 74 de la Constitución Politica de los Estados Unidos Mexicanos,los cuales mensionaremos de manera resumida:
I. Expedir el Bando Solemne para a dar a conocer en toda la República de Presidente electo que hubiere hecho el Tribunal Electoral.
II. Coordinar y evaluar las funciones de la Auditoria Superior de la Federación.
III. Ratificar el nombramiento que realice el Presidente de la República que haga del
Secretario en materia de Hacienda.
IV. Aprobar anualmente el Presupuesto de Egresos de la Federación.
V. Declarar si ha o no lugar a proceder penalmente contra los Servidores Públicos
que hubieren incurrido en un delito.
VI. Revisar la Cuenta Pública del año anterior, con el objeto de evaluar los resultados
de la gestión financiera.
VII.Aprobar el Plan Nacional de Desarrollo

Cámara de Senadores:en el art. 76 de la Constitución Politica de los Estados Unidos Mexicanos, se mensionan las facultades de los senadores 
las cuales mensionaremos de manera resumida:
I. Analizar la Política Exterior desarrollada por el Ejecutivo Federal, además de aprobar Tratados Internacionales y Convenciones Diplomáticas.
II. Ratificar los nombramientos de los Secretarios de Estado, en caso de que se
opte por un gobierno de coalición, con excepción de los titulares de los ramos
de Defensa Nacional y Marina; del Secretario Responsable del Control Interno
del Ejecutivo Federal, del Secretario de Relaciones; de los Embajadores y Cónsules Generales; de los empleados superiores del ramo de Relaciones; de los
integrantes de los órganos colegiados encargados de la regulación en materia
de Telecomunicaciones, Energía, competencia Económica, y Coroneles y demás
jefes superiores del Ejército, Armada y Fuerza Aérea.
III. Autorizar la salida de tropas nacionales fuera de los límites del país.
IV. Dar su consentimiento al Presidente de la República para disponer de la Guardia
Nacional.
V. Declarar, cuando hayan desaparecido todos los Poderes Constitucionales en una
Entidad Federativa, en su caso nombrarle a un titular del Poder Ejecutivo Provisional.
VI. Resolver las cuestiones políticas que surjan entre los poderes de una entidad
federativa cuando alguno de ellos ocurra con ese fin al Senado, o cuando con
motivo de dichas cuestiones se haya interrumpido el orden constitucional, mediante un conflicto de armas.
VII. Erigirse en jurado de sentencia para conocer de un Juicio Político de las faltas u
omisiones que cometan los servidores públicos.
VIII. Designar a los Ministros de la Suprema Corte de Justicia de la Nación, de entre
la terna que someta a su consideración el Presidente de la República.

Facutades del congreso: desde el punto de vista de manera material se puede decir que suelen ser:
Legislativas
Ejecutivas
Jurisdiccionales 

Poder Judicial 
El art. 105 de la Constitución Política de los Estados Unidos Mexicanos, establece las facultade del Poder Legislativo que son, 
Controversias Constitucionales 
Acciones de Inconstitucionalidad 
Recursos de Apelación 


